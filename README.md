# gurps4e

Foundry Virtual TableTop support for the Generic Universal Roleplaying System, (any) Edition.

This game aid is intended to provide you with the ability to play GURPS on Foundry VTT but will still require the use of your official GURPS materials.

GURPS is a trademark of Steve Jackson Games, and its rules and art are copyrighted by Steve Jackson Games. All rights are reserved by Steve Jackson Games. This game aid is the original creation of James B Huddleston and is released for free distribution, and not for resale, under the permissions granted in the <a href="http://www.sjgames.com/general/online_policy.html">Steve Jackson Games Online Policy</a>.

Official material for GURPS is published by Steve Jackson Games (http://www.sjgames.com) and may be purchased through Warehouse 23 (http://www.warehouse23.com).

If you would like to support this effort, you may do so here; https://www.patreon.com/jbhuddleston
