GURPS Project for Foundry VTT

Goals
- The primary goal of this project is to permit people to play GURPS online as easily as if they were sitting around a table. The facts of digital displays and simplification of many actions through automation will permit much more but are a stretch goal rather than the primary one.
- A secondary goal is that ready-made characters be able to be imported into the sheet from a recognised character generator output. Whether this is a JSON input format to which any character generator may create an output, or an input of raw GCS files, is up for discussion.
- A tertiary goal is to be able to play as much as possible while acting through a token on the playing surface. This minimizes clutter and will likely speed up or smooth play.

General Design Notes
- Do not try to display all information about an object, just enough to be useful for reference or to identify it among its' peers. Employ a means to see detailed information about a summarized object by interacting with it in some way that takes positive action; like the Foundry chat tooltip behavior. 
- Localisation is to be employed for all system-generated labels.
- Code organisation. Follow the pattern of WFRP in general. Hooks are to be given their own files so that all actions taking place because of them are consolidated.

Works in Progress
- the Size Speed Range ruler module by Exxar
	- is now integrated in ./lib
	- a revision will allow storage of a (modifier, name) pair by the sheet
		- suggested behaviour was storage of accumulated distance on every click

- Hotkey Movement model
	- make it possible to override the default Foundry movement model so that the leftmost keys or number keypad may be used to move a token: forward, backward, turn right, turn left, sidestep forward right and left and sidestep rearward both right and left.
	- need to be careful of existing hotkeys suppported by Foundry
		- note that existing settings have the token facing downscreen by default
	- eventually would like to be able to track the number of hexes moved thus far.
		- system setting for RAW tracking of turns or just hexes
		- reset as the token reaches the top of the combat tracker
		- reset to origin on request

- the Roll Library and Rendering by Exxar
	- is now integrated in ./lib
	- works for both 0.6.x and 0.7.x

Action processing
- currently one sets the gmod then rolls the attack, defence, check or skill.
	- this will continue to be possible and augmented by further options as the roll-library is fully integrated
- another process needs implementation where:
	- the roll is initiated and a dialog specific to the roll type is presented
	- the gmod is top of the list of user-definable modifiers also filtered for the roll type
	- the user selects the appropriate modifiers and rolls.
	- this process may be preceded by a macro where the action and target are selected
		- there would be a different macro for each type of process to minimise user-actions required

Macro options for a selected token
- each type of action possible to be taken by an actor should be accessible through a macro
	- select a target then choose the attack macro and a dialog appears to select which one from the list of attacks owned by the actor associated with the token.
	- or start the macro, select from the list of actor actions of that type available, then choose a target or targets (area effect?) to generate the type-specific dialog to prompt for modifiers. If this can be done dynamically in a single dialog, that would be prefereable perhaps.
	- skills, techniques, spells and ritual magic spells would act in the same fashion.
- discuss the possibility of triggering the target to choose an active defence in response to a successful attack. Maybe some interaction by dragging chat results to a token?

Status Effect linkage
- there may be core support for this coming
- All status effects available to the token shall be supported by the sheet.
- toggle a status on the token and it is toggled on the sheet as well, thus affecting die rolls.
- some status are health or fatigue specific and should be triggered automatically.
- not sure about crippling injury being automatic based on damage to a location and system settings (house rules) selected.

Localisation
- the framework for it is in place. Expand upon it so that any system generated text is drawn from it.

Icons
- make better icons for status effects and other applications

GCS Import
- many conflicting thoughts about this so bear with me.
- I want to maintain flexibility for Optional Rules not supported by GCS, House Rules and, hopefully, use for GURPS 3e. The first and second will be supported; the latter may be supported.
- GCS has just added a section in the JSON for "third party" data. Not sure how it would work but this may allow us to persist data only we are using.
- GCS output templates necessarily calculate all the things they know to calculate according to RAW. Unless this can be overridden in progress or overwritten later, we cannot use the output templates except in the cases of things that will always be calculated anyway.
- a Foundry actor with many items takes a little bit longer to react, so minimising the number of items is a good thing.
- Foundry operates best from attributes that are defined in the template in advance, or second best if defined at runtime, finally is attributes stored in items. However, to achieve flexibility, any data that is not contained/used by all actors should be stored in items; this can be written to the model at runtime if desired.
- Values for Foundry items that need to be updated must be updated in the item, not in the runtime-generated data from that item or it will be erased at best or cause an endless update loop at worst. This is not a significant performance hit as it only occurs when the item is being updated, not used in play.
- Only data defined in the template, or in items, is persisted by the system.
- see the Foundry article on chat message for info on macro access to variables. They have probably oriented it specifically to the DnD5e system in their description because any variable defined at the root level of an actor is accessible, even those added at runtime. There may be some special behaviour associated with the specific fields they define, however.
- A redesign of the template is called for, to store as much as possible in the template even though it may be generated/overwritten by an import or addition of an item.
	- attributes (as checks), including Lifting ST, Striking ST, and so on.
	- all the common checks, even those not recorded by GCS, like Death, Consciousness and Knockdown & Stun.
	- encumbrance levels need to be defined by their effects on other activities
	- don't know how to deal with the lack of info on hit locations but have created a library of the LTIA hit locations and armour types with the required data in the notes section in JSON format.
	- movement modes may need to be created as new items, but that can wait perhaps.
	- equipment needs to be scanned for the effects they produce and those effects imported. We should assess whether or not a pile of equipment will gum up the works or is largely ignored. Remember that the actor and his items are written to the text-based DB every time a change is made to something on the template or an item.
- we might try a RAW import for testing and quick support for those who play RAW. This could use an output template as is. We may be able to add support for Conditional Injury and other Pools through system settings and dropping items of those types after the import.
- The more involved model importing from the GCS file can be the alternate, even though it is always the one I will use.
- many more thoughts to come...