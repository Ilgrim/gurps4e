import { SuccessRoll, SuccessRollRenderer, DamageRoll, DamageRollRenderer, ReactionRoll, ReactionRollRenderer } from "../../lib/gurps-foundry-roll-lib/gurps-foundry-roll-lib.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class gurpsActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["gurps4e", "sheet", "actor"],
      template: "systems/gurps4e/templates/actor/actor-sheet.html",
      width: 514,
      height: 425,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "stats" }]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    let data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];

    let actor = data.actor;
    let actordata = actor.data;

    actor.items = actor.items.sort((item1, item2) => {
      if (item1.name < item2.name) return -1;
      if (item1.name > item2.name) return 1;
      return 0;
    });

    actordata.primaryattributes = {};
    actordata.equipment = {};
    actordata.hitlocations = {};
    actordata.skills = {};
    actordata.techniques = {};
    actordata.spells = {};
    actordata.ritualmagicspells = {};
    actordata.checks = {};
    actordata.advantages = {};
    actordata.disadvantages = {};
    actordata.perks = {};
    actordata.quirks = {};
    actordata.meleeattacks = {};
    actordata.rangedattacks = {};
    actordata.defences = {};
    actordata.pools = {};
    actordata.modifiers = {};

    // create collections of the items by type
    for (const item of actor.items) {
      switch (item.type) {
        case "Primary-Attribute": {
          actordata.primaryattributes[item._id] = item;
          break;
        }
        case "Equipment": {
          actordata.equipment[item._id] = item;
          break;
        }
        case "Hit-Location": {
          actordata.hitlocations[item._id] = item;
          break;
        }
        case "Rollable": {
          switch (item.data.category) {
            case "skill": {
              actordata.skills[item._id] = item;
              break;
            }
            case "technique": {
              actordata.techniques[item._id] = item;
              break;
            }
            case "spell": {
              actordata.spells[item._id] = item;
              break;
            }
            case "rms": {
              actordata.ritualmagicspells[item._id] = item;
              break;
            }
            case "check": {
              actordata.checks[item._id] = item;
              break;
            }
          }
          break;
        }
        case "Trait": {
          switch (item.data.category) {
            case "advantage": {
              actordata.advantages[item._id] = item;
              break;
            }
            case "diadvantage": {
              actordata.disadvantages[item._id] = item;
              break;
            }
            case "perk": {
              actordata.perks[item._id] = item;
              break;
            }
            case "quirk": {
              actordata.quirks[item._id] = item;
              break;
            }
          }
          break;
        }
        case "Melee-Attack": {
          actordata.meleeattacks[item._id] = item;
          break;
        }
        case "Ranged-Attack": {
          actordata.rangedattacks[item._id] = item;
          break;
        }
        case "Defence": {
          actordata.defences[item._id] = item;
          break;
        }
        case "Pool": {
          actordata.pools[item._id] = item;
          break;
        }
        case "Modifier": {
          actordata.modifiers[item._id] = item;
          break;
        }
      }
    }

    data.skills = data.items.filter(i => (i.type == "Rollable" && i.data.category == "skill"));
    return data;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");

      // uncheck Pools before deleting them
      let item = this.actor.getOwnedItem(li.data("itemId"));
      if (item.data.data.inHeader) {
        item.update({ "data.inHeader": false });
      }
      if (item.data.data.formula) {
        delete this.actor.data.data.dynamic[this.actor.slugify(item.data.name)];
      }
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Rollable checks.
    html.find('.rollable').click(this._onRoll.bind(this));

    // Item Input Changes.
    html.find('.iteminput').change(this._onInputChange.bind(this));

    // Plus - Minus check
    html.find('.plus').click(this._onPlusMinus.bind(this));
    html.find('.minus').click(this._onPlusMinus.bind(this));

    // Item Dragging
    let handler = ev => this._onDragStart(ev);
    html.find('.fa-anchor').each((i, li) => {
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, false);
    });
  }

  /* -------------------------------------------- */

  /** @override */
  _onDragStart(event) {
    // changed it to target the parentElement of the event target
    const li = event.currentTarget.parentElement;
    // the rest is the same
    const item = this.actor.getOwnedItem(li.dataset.itemId);
    const dragData = {
      type: "Item",
      actorId: this.actor.id,
      data: item.data
    };
    if (this.actor.isToken) dragData.tokenId = this.actor.token.id;
    event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
  }

  /**
   * Handle the behaviour of the plus and minus 'buttons' related to a label.
   * @param {Event} event   The originating click event
   * @private
   */
  async _onPlusMinus(event) {
    event.preventDefault();
    let field = event.currentTarget.firstElementChild;
    let actorData = this.actor.data.data;
    let fieldName = field.name;
    let change = parseInt(field.value);
    var value;
    var fieldValue;
    var gurpsItem;

    switch (fieldName) {
      case "gmod": {
        fieldValue = "data.gmod.value";
        value = change + actorData.gmod.value;
        this.actor.update({ [fieldValue]: value });
        break;
      }
      case "IS": {
        fieldValue = "data.is.value";
        value = change + actorData.is.value;
        this.actor.update({ [fieldValue]: value });
        break;
      }
      case "DLtH": {
        fieldValue = "data.dlth.value";
        value = change + actorData.dlth.value;
        this.actor.update({ [fieldValue]: value });
        break;
      }
      case "FPmax": {
        fieldValue = "data.tracked.fp.max";
        value = (game.settings.get("gurps4e", "useHalfFPIncrements") ? change / 2 : change) + actorData.tracked.fp.max;
        // await this.actor.update({ [fieldValue]: value });
        gurpsItem = await this.actor.getOwnedItem(actorData.tracked.fp.id);
        await gurpsItem.update({ ['data.max']: value });
        await gurpsItem.update({ ['data.min']: -value });
      }
      case "FP": {
        fieldValue = "data.tracked.fp.value";
        value = (game.settings.get("gurps4e", "useHalfFPIncrements") ? change / 2 : change) + actorData.tracked.fp.value;
        // await this.actor.update({ [fieldValue]: value });
        gurpsItem = await this.actor.getOwnedItem(actorData.tracked.fp.id);
        await gurpsItem.update({ ['data.value']: value });
        this.actor.setConditions(value, fieldValue);
        break;
      }
      default: {
        for (let [id, tracked] of Object.entries(actorData.tracked)) {
          if (id === "fp") continue;
          if (fieldName.toLowerCase().startsWith(id)) {
            gurpsItem = await this.actor.getOwnedItem(actorData.tracked[id].id);
            if (fieldName.endsWith("max")) {
              // do max things
              fieldValue = `data.tracked.${id}.max`;
              value = change + actorData.tracked[id].max;
              // await this.actor.update({ [fieldValue]: value });
              await gurpsItem.update({ ['data.max']: value });
              if (id === "hp") await gurpsItem.update({ ['data.min']: -value * 5 });
            }
            fieldValue = `data.tracked.${id}.value`;
            value = change + actorData.tracked[id].value;
            // await this.actor.update({ [fieldValue]: value });
            await gurpsItem.update({ ['data.value']: value });
            this.actor.setConditions(value, fieldValue);
          }
        }
      }
    }
  }

  /** 
   * Override the _onDrop method to open the dropped item for editing.
   * @param {Event} event   The originating drop event
   * @private
   */
  /** @override */
  async _onDrop(event) {
    // wait for the item to be copied to the actor
    const item = await super._onDrop(event);
    const gurpsitem = this.actor.getOwnedItem(item._id);
    if (gurpsitem.data.type == "Container") this._createItems(gurpsitem.data.data.notes);
    gurpsitem.sheet.render(true);
  }

  /**
 * Handle the Item Input Change Event
 * @param {Event} event   The originating click event
 * @private
 */
  _onInputChange(event) {
    event.preventDefault();
    const target = event.currentTarget;
    // get the item id
    const itemId = target.parentElement.attributes["data-item-id"].value;
    // get the name of the changed element
    const dataname = target.attributes["data-name"].value;
    // get the new value
    const value = (target.type === "checkbox") ? target.checked : target.value;
    // Get the original item.
    const item = this.actor.getOwnedItem(itemId);
    // update the item with the new value for the element
    item.update({ [dataname]: value });
  }

  /**
 * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
 * @param {Event} event   The originating click event
 * @private
 */
  _onItemCreate(event) {
    event.preventDefault();
    const scriptdata = this.actor.data.data.itemscript;
    this._createItems(scriptdata);
  }

  /**
   * Handle item creation from multiple possible sources
   * @param {*} scriptdata
   * @private
   */
  async _createItems(scriptdata) {
    var items = JSON.parse(scriptdata);
    console.log(items);
    for (var item of items) {
      await this.actor.createOwnedItem(item);
    }
  }

  /**
  * Handle clickable rolls.
  * @param {Event} event   The originating click event
  * @private
  */
  _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    const actorData = this.actor.data.data;
    const actorMods = {};
    let tempMods = [];

    for (const item of this.actor.data.items) {
      if (item.type === "Modifier") {
        actorMods[item._id] = item;
      }
    }

    // preload gmod (and dmod for defences)
    tempMods.push({ modifier: actorData.gmod.value, description: 'global modifier' });

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (let [id, mod] of Object.entries(actorMods)) {
      if (mod.data.inEffect) {
        switch (dataset.type) {
          case "check":
          case "skill":
            if (mod.data.success) tempMods.push({ modifier: mod.data.value, description: mod.name });
            break;
          case "attack":
            if (mod.data.attack) tempMods.push({ modifier: mod.data.value, description: mod.name });
            break;
          case "defence":
            if (mod.data.defence) tempMods.push({ modifier: mod.data.value, description: mod.name });
            break;
          case "reaction":
            if (mod.data.reaction) tempMods.push({ modifier: mod.data.value, description: mod.name });
            break;
          case "damage":
            if (mod.data.damage) tempMods.push({ modifier: mod.data.value, description: mod.name });
            break;
        }
        // check to see if the modifier is a specific case
        if (mod.data.specific) {
          // if it is specific, get the array of cases it applies to
          const cases = mod.data.targets.split(",").map(word => word.trim());
          for (const target of cases) {
            // test the array against dataset.name for a match
            if (dataset.name.startsWith(target)) {
              // push the modifier if so
              tempMods.push({ modifier: mod.data.value, description: mod.name });
              continue;
            }
          }
        }
      }
    }

    if (!(dataset.type === "defence" || dataset.type === "damage" || dataset.type === "reaction")) dataset.type = "skill";

    const executeRoll = (roll, renderer) => {
      roll.roll();
      const flavor = (actorData.showRollFlavour) ? dataset.label || null : null;
      renderer.render(roll, { template: 'systems/gurps4e/lib/gurps-foundry-roll-templates/templates/roll.html' }, { flavor }).then((html) => {
        ChatMessage.create({ content: html, speaker: ChatMessage.getSpeaker({ actor: this.actor }), type: CONST.CHAT_MESSAGE_TYPES.OTHER });
      });
    }

    const executeSuccessRoll = modList => {
      const trait = (actorData.showRollTrait) ? dataset.name || null : null;
      executeRoll(SuccessRoll.fromData({ level: dataset.level, trait, modList }), new SuccessRollRenderer());
    };

    const executeReactionRoll = modList => {
      executeRoll(ReactionRoll.fromData({ modList: modList }), new ReactionRollRenderer());
    };

    const prepareModList = mods => mods.map(mod => ({ ...mod, modifier: parseInt(mod.modifier, 10) })).filter(mod => mod.modifier !== 0);

    var modList = prepareModList(tempMods);

    if (dataset.type === 'skill' || dataset.type === 'defence') {
      executeSuccessRoll(modList);
    } else if (dataset.type === 'reaction') {
      executeReactionRoll(modList);
    } else if (dataset.type === 'damage') {
      const rollMatch = dataset.roll.match(/^([1-9][0-9]*)d6([+-][0-9]+)?$/);
      executeRoll(DamageRoll.fromData({ dice: rollMatch[1], adds: rollMatch[2] || '', modList }), new DamageRollRenderer());
    } else {
      console.log("Rollable element triggered with an unsupported data-type (supported types are 'skill', 'damage', 'reaction' and 'defence'");
    }

    this.actor.update({ ["data.gmod.value"]: 0 });
  }

}
