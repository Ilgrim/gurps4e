/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class gurpsActor extends Actor {

  /**
   * Override the create() function to provide additional GURPS4e functionality.
   * 
   * It does nothing at the moment. Just leaving it here in case I want to do something later.
   *
   * @param {Object} data        Barebones actor data which this function adds onto.
   * @param {Object} options     (Unused) Additional options which customize the creation workflow.
   *
   */
  static async create(data, options) {
    super.create(data, options); // Follow through the the rest of the Actor creation process upstream
  }

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {
    // store the existing values in dynamic
    let temp = this.data.data.dynamic;

    super.prepareData();

    // the refreshed/cleaned data based on the template
    const actorData = this.data;

    // restore the dynamic data from previous calculations
    actorData.data.dynamic = temp;

    actorData.data.useConditionalInjury = game.settings.get("gurps4e", "useConditionalInjury");
    actorData.data.showRollFlavour = game.settings.get("gurps4e", "showRollFlavour");
    actorData.data.showRollTrait = game.settings.get("gurps4e", "showRollTrait");

    // deal with all of the calculations and update all values in dynamic
    this._prepareCharacterData(actorData);

    // let's see what we did
    console.log("after calling _prepareCharacterData");
    console.log(actorData);
  }

  /**
   * Handle how changes to a Token attribute bar are applied to the Actor.
   * This allows for game systems to override this behavior and deploy special logic.
   * @param {string} attribute    The attribute path
   * @param {number} value        The target attribute value
   * @param {boolean} isDelta     Whether the number represents a relative change (true) or an absolute change (false)
   * @param {boolean} isBar       Whether the new value is part of an attribute bar, or just a direct value
   * @return {Promise}
   */
  async modifyTokenAttribute(attribute, value, isDelta = false, isBar = true) {
    const current = getProperty(this.data.data, attribute);
    if (isBar) {
      if (isDelta) value = Math.clamped(current.min, Number(current.value) + value, current.max);

      // TODO: insert a call to the status check method
      this.setConditions(value, attribute);

      return this.update({ [`data.${attribute}.value`]: value });
    } else {
      if (isDelta) value = Number(current) + value;

      // TODO: insert a call to the status check method
      this.setConditions(value, attribute);

      return this.update({ [`data.${attribute}`]: value });
    }
  }

  slugify(text) {
    return text
      .toString()                     // Cast to string
      .toLowerCase()                  // Convert the string to lowercase letters
      .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
      .trim()                         // Remove whitespace from both sides of a string
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-');        // Replace multiple - with single -
  }

  /**
   * Replace referenced data attributes in the formula with the syntax `@attr` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    let dataRgx = new RegExp(/@([a-z.0-9_\-]+)/gi);
    return formula.replace(dataRgx, (match, term) => {
      let value = getProperty(this.data.data.dynamic, `${term}.value`);
      return (value) ? String(value).trim() : "0";
    });
  }
  /**
 * Prepare minchar type specific data
 * 
 */
  _prepareCharacterData(actorData) {

    // write primary attributes to dynamic first
    for (const item of actorData.items) {
      if (item.type == "Primary-Attribute") {
        // write the item to attributes
        let trackId = this.slugify(item.name);
        actorData.data.dynamic[trackId] = {
          id: trackId,
          name: item.name,
          value: parseInt(item.data.value),
        };
      }
    }
    // modify primary attributes as necessary
    for (const item of actorData.items) {
        // check to see if the modifier will modify primary attributes
        if (item.data.primary) {
          // to which primary attributes does it apply
          const cases = item.data.targets.split(",").map(word => word.trim().toLowerCase());
          // add its' value to each of those primary attributes
          for (const target of cases) {
            if (actorData.data.dynamic[target]) actorData.data.dynamic[target].value += item.data.value;
          }
        }
    }
    // calculate value from formula
    for (const item of actorData.items) {
      if (item.data.formula) {
        if (!isNaN(parseInt(item.data.formula))) { // the formula is a number
          item.data.value = parseInt(item.data.formula);
        } else { // the formula is an arithmetic expression
          item.data.value = Math.round(eval(this._replaceData(item.data.formula).replace(/[^-()\d/*+.]/g, '')));
        }
      }
    }

    // inspect the Pools to see if they need to be added or removed
    for (const item of actorData.items) {
      switch (item.type) {
        case "Pool": {
          let trackId = item.data.abbr.toLowerCase();
          if (item.data.inHeader) { // checked, write it
            actorData.data.tracked[trackId] = {
              id: item._id,
              abbr: item.data.abbr,
              name: item.name,
              state: item.data.state,
              min: parseInt(item.data.min),
              value: parseInt(item.data.value),
              max: parseInt(item.data.max)
            };
          } else { // unchecked, remove 
            delete actorData.data.tracked[trackId];
          }
          break;
        }
        case "Melee-Attack":
        case "Ranged-Attack":
        case "Defence":
        case "Rollable": {

          // write the item to dynamic
          let trackId = this.slugify(item.name);
          actorData.data.dynamic[trackId] = {
            id: trackId,
            name: item.name,
            value: item.data.value,
          };
          break;
        }
      }
    }

  }

  setConditions(newValue, attrName) {
    var attrValue;
    var attrMax;
    var attrState;
    var gurpsItem;
    let tracked = this.data.data.tracked;
    let attr = attrName.split('.')[2];

    if (attr === "hp") { // Hit points update

      // Assign the variables
      if (attrName.includes('.max')) {
        attrMax = newValue;
        attrValue = tracked.hp.value;
      } else {
        attrValue = newValue;
        attrMax = tracked.hp.max;
      }
      let ratio = attrValue / attrMax;
      // set the limits
      switch (Math.trunc(ratio)) {
        case 0: {
          if (ratio <= 0) { // collapse
            attrState = '[C]';
            break;
          } else if (attrValue < (attrMax / 3)) { // reeling
            attrState = '[R]';
            break;
          }
          // healthy, no break
        }
        case 1: { // healthy
          attrState = '[H]';
          break;
        }
        case -1: { // death check at -1
          attrState = '[-X]';
          break;
        }
        case -2: { // death check at -2
          attrState = '[-2X]';
          break;
        }
        case -3: { // death check at -3
          attrState = '[-3X]';
          break;
        }
        case -4: { // death check at -4
          attrState = '[-4X]';
          break;
        }
        default: { // dead
          attrState = '[DEAD]';
          break;
        }
      }

    } else if (attr === "fp") { // Fatigue points update

      // Assign the variables
      if (attrName.includes('.max')) {
        attrMax = newValue;
        attrValue = tracked.fp.value;
      } else {
        attrValue = newValue;
        attrMax = tracked.fp.max;
      }
      let ratio = attrValue / attrMax;
      // set the limits
      switch (Math.trunc(ratio)) {
        case 0: {
          if (ratio <= 0) { // collapse
            attrState = '[C]';
            break;
          } else if (attrValue < (attrMax / 3)) { // tired
            attrState = '[T]';
            break;
          }
          // fresh, no break
        }
        case 1: { // fresh
          attrState = '[F]';
          break;
        }
        default: { // unconscious
          attrState = '[UNC]';
          break;
        }
      }
    }
    gurpsItem = this.getOwnedItem(tracked[attr].id);
    gurpsItem.update({ ['data.state']: attrState });
  }
}
