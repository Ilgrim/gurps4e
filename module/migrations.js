export class Migration {

  static async migrateWorld() {
    ui.notifications.notify("Beginning Migration to GURPS4E 1.0")
    for (let i of game.items.entities) {
      await i.update(this.migrateItemData(duplicate(i.data)));
    }

    for (let a of game.actors.entities) {
      await this.migrateActorData(a);
    }

    for (let p of game.packs) {
      if (p.metadata.entity == "Item" && p.metadata.package == "world")
        p.getContent().then(async (items) => {
          items.forEach(async (i) => {
            if (i.type == "weapon") {
              await p.updateEntity(this.migrateItemData(i.data));
            }
          })
        })

      if (p.metadata.entity == "Actor" && p.metadata.package == "world") {
        p.getContent().then(async (actors) => {
          actors.forEach(async (a) => {
            p.updateEntity(await this.migrateActorData(a))
          })
        })
      }

    }
    ui.notifications.notify("Migration to GURPS4E 1.0 Finished")

    game.settings.set("gurps4e", "systemMigrationVersion", game.system.data.version)

  }

  static async migrateActorData(actor) {
    let actorItems = actor.items;
    let actorData = actor.data.data;
    for (let i of actorItems) {
      await actor.updateEmbeddedEntity("OwnedItem", this.migrateItemData(i.data));
    }

    // create defence items
    for (let [id, defence] of Object.entries(actorData.defences)) {
      const type = "Defence";
      const data = {};
      data.level = defence.value;
      data.category = id;
      const name = defence.label;
      const itemData = {
        name: name,
        type: type,
        data: data
      };
      await actor.createOwnedItem(itemData);
    }
    delete actorData.defences;

    // create primaryAttributes as checks
    for (let [id, item] of Object.entries(actorData.primaryAttributes)) {
      const type = "Rollable";
      const data = {};
      data.level = item.value;
      data.category = "check";
      const name = item.abbr;
      const itemData = {
        name: name,
        type: type,
        data: data
      };
      await actor.createOwnedItem(itemData);
    }
    delete actorData.primaryAttributes;
    delete actorData.dmod;

    // create checks
    for (let [id, item] of Object.entries(actorData.checks)) {
      if (item.label == "") continue;
      const type = "Rollable";
      const data = {};
      data.level = item.value;
      data.category = "check";
      const name = item.label;
      const itemData = {
        name: name,
        type: type,
        data: data
      };
      await actor.createOwnedItem(itemData);
    }
    delete actorData.checks;

    // create skills 
    for (let [id, item] of Object.entries(actorData.skills)) {
      if (item.label == "") continue;
      const type = "Rollable";
      const data = {};
      data.level = item.value;
      data.category = "skill";
      const name = item.label;
      const itemData = {
        name: name,
        type: type,
        data: data
      };
      await actor.createOwnedItem(itemData);
    }
    delete actorData.skills;

    // create modifiers 
    for (let [id, item] of Object.entries(actorData.modifiers)) {
      if (item.label == "") continue;
      const type = "Modifier";
      const data = {};
      data.modifier = item.value;
      data.attack = true;
      data.inEffect = item.inEffect;
      const name = item.label;
      const itemData = {
        name: name,
        type: type,
        data: data
      };
      await actor.createOwnedItem(itemData);
    }
    delete actorData.modifiers;

    // create attacks 
    for (let [id, item] of Object.entries(actorData.attacks)) {
      if (item.wpnName == "") continue;
      const type = "Melee-Attack";
      const data = {};
      data.level = item.skill;
      data.damage = item.formula;
      data.damageType = item.woundType
      const name = item.wpnName;
      const itemData = {
        name: name,
        type: type,
        data: data
      };
      await actor.createOwnedItem(itemData);
    }
    delete actorData.attacks;

    return actor.data
  }

  static migrateItemData(itemData) {
    if (itemData.type == "weapon")
      return this.migrateWeaponData(itemData)
    else
      return itemData;
  }

  static migrateWeaponData(weaponData) {
    if (!weaponData.data.damage.value) {
      let isMelee = GURPS4E.groupToType[weaponData.data.weaponGroup.value] == "melee"

      if (isMelee)
        weaponData.data.damage.value = weaponData.data.damage.meleeValue;
      else
        weaponData.data.damage.value = weaponData.data.damage.rangedValue;
    }
    return weaponData
  }
}